import React from 'react';
import PropTypes from 'prop-types';
import * as style from './Details.style';

const Details = ({ closeButton, image, name, details }) => (
  <div className={style.details}>
    <div className={style.details__image}>
      {image}
      <div className={style.details__imageCloseButton}>{closeButton}</div>
    </div>
    <div className={style.details__content}>
      <div className={style.details_header}>
        <div className={style.details__name}>{name}</div>
        <div className={style.details__contentCloseButton}>{closeButton}</div>
      </div>
      <div>{details}</div>
    </div>
  </div>
);

Details.propTypes = {
  closeButton: PropTypes.node,
  image: PropTypes.node,
  name: PropTypes.node,
  details: PropTypes.node,
};

export default Details;
