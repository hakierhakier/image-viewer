import { css } from 'emotion';

export const details = css`
  display: flex;
  height: 100vh;
  width: 100vw;

  @media (max-width: 800px) {
    flex-direction: column;
  }
`;

export const details__image = css`
  align-items: center;
  background: #000;
  display: flex;
  height: 100%;
  justify-content: center;
  position: relative;
  width: calc(100vw - 400px);

  @media (max-width: 800px) {
    width: 100%;
  }
`;

export const details__content = css`
  box-sizing: border-box;
  height: 100%;
  padding: 10px;
  width: 400px;

  @media (max-width: 800px) {
    width: 100%;
  }
`;

export const details_header = css`
  display: flex;
  align-items: center;
`;

export const details__name = css`
  flex-grow: 1;
`;

export const details__contentCloseButton = css`
  @media (max-width: 800px) {
    display: none;
  }
`;

export const details__imageCloseButton = css`
  display: none;
  position: absolute;
  right: 20px;
  top: 20px;

  @media (max-width: 800px) {
    display: block;
  }
`;
