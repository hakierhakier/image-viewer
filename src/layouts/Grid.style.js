import { css } from 'emotion';

export const gridLayout = css`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  width: 100%;
`;

export const gridLayout__item = css`
  margin: 6px;
`;
