import React from 'react';
import * as style from './CenteredContent.style';

const CenteredContent = ({ content }) => <div className={style.centeredContent}>{content}</div>;

export default CenteredContent;
