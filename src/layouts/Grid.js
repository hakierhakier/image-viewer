import React, { Children } from 'react';
import PropTypes from 'prop-types';
import * as style from './Grid.style';

const Grid = ({ items }) => (
  <div className={style.gridLayout}>
    {Children.map(items, child => (
      <div className={style.gridLayout__item}>{child}</div>
    ))}
  </div>
);

Grid.propTypes = {
  items: PropTypes.node,
};

export default Grid;
