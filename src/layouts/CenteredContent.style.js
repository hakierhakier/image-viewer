import { css } from 'emotion';

export const centeredContent = css`
  align-items: center;
  display: flex;
  height: 100vh;
  justify-content: center;
`;
