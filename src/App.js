import React from 'react';
import { useMachine } from '@xstate/react';
import appMachine from './state-machine-app';
import Details from './views/Details';
import Fetching from './views/Fetching';
import Grid from './views/Grid';

function App() {
  const [{ value, context }, send] = useMachine(appMachine);

  const { ViewComponent, props } = {
    fetching: {
      ViewComponent: Fetching,
      props: {},
    },
    grid: {
      ViewComponent: Grid,
      props: {
        onSelect: id => {
          send({ type: 'SELECTED', payload: { id } });
        },
      },
    },
    details: {
      ViewComponent: Details,
      props: {
        onClose: () => {
          send({ type: 'CLOSED' });
        },
      },
    },
  }[value];

  return <ViewComponent data={context.viewData} {...props} />;
}

export default App;
