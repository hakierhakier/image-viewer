import React from 'react';
import PropTypes from 'prop-types';
import * as style from './Anchor.style';

const Anchor = ({ href, children, onClick }) => (
  <a className={style.anchor} href={href} onClick={onClick}>
    {children}
  </a>
);

Anchor.propTypes = {
  href: PropTypes.string,
  children: PropTypes.node,
  onClick: PropTypes.func,
};

export default Anchor;
