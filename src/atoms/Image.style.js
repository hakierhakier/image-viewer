import { css } from 'emotion';

export const image = css`
  max-height: 100%;
  max-width: 100%;
`;
