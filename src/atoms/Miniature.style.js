import { css } from 'emotion';

export const miniature = css`
  align-items: center;
  background: repeating-linear-gradient(45deg, #fff, #fff 2px, #eee 2px, #ebebeb 4px);
  box-shadow: 1px 2px 4px #bbb;
  cursor: pointer;
  display: flex;
  height: 200px;
  justify-content: center;
  position: relative;
  transition: box-shadow 0.2s ease;
  width: 200px;

  &:hover {
    box-shadow: 6px 10px 20px #bbb;
  }
`;

export const miniature__title = css`
  background: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(200, 200, 200, 0.8));
  bottom: 0;
  position: absolute;
  text-align: right;
  width: 100%;
`;
