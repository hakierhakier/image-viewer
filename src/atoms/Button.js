import React from 'react';
import PropTypes from 'prop-types';
import * as style from './Button.style';

const Button = ({ children, onClick }) => (
  <button className={style.button} onClick={onClick}>
    {children}
  </button>
);

Button.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func,
};

export default Button;
