import React from 'react';
import PropTypes from 'prop-types';
import * as style from './Miniature.style';

const Miniature = ({ title, children }) => (
  <div className={style.miniature}>
    {children}
    <div className={style.miniature__title}>{title}</div>
  </div>
);

Miniature.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node,
};

export default Miniature;
