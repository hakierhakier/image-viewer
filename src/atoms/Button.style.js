import { css } from 'emotion';

export const button = css`
  background: #fff;
  border: 0;
  color: inherit;
  display: block;
  padding: 10px;
`;
