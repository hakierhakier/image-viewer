import React from 'react';
import PropTypes from 'prop-types';
import * as style from './Image.style';

const Image = ({ src, alt }) => <img className={style.image} src={src} alt={alt} />;

Image.propTypes = {
  src: PropTypes.string,
  alt: PropTypes.string,
};

export default Image;
