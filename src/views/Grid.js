import React from 'react';
import PropTypes from 'prop-types';
import Anchor from '../atoms/Anchor';
import Image from '../atoms/Image';
import Miniature from '../atoms/Miniature';
import GridLayout from '../layouts/Grid';

const Grid = ({ data, onSelect }) => (
  <GridLayout
    items={data.map((item, index) => (
      <Anchor
        onClick={e => {
          e.preventDefault();
          onSelect(item.id);
        }}
        href={item.img}
      >
        <Miniature key={`${item.img}.${item.title}.${index}`} title={item.title}>
          <Image src={item.img} alt={item.title} />
        </Miniature>
      </Anchor>
    ))}
  />
);

Grid.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({ img: PropTypes.string, title: PropTypes.string })),
  onSelect: PropTypes.func,
};

Grid.defaultProps = {
  data: [],
  onSelect: () => {},
};

export default Grid;
