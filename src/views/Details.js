import React from 'react';
import PropTypes from 'prop-types';
import Button from '../atoms/Button';
import Image from '../atoms/Image';
import DetailsLayout from '../layouts/Details';

const Details = ({ data, onClose }) => (
  <DetailsLayout
    closeButton={<Button onClick={onClose}>&#x2573;</Button>}
    image={<Image src={data.img} alt={data.title} />}
    name={<h2>{data.title}</h2>}
    details={<p>{data.details}</p>}
  />
);

Details.propTypes = {
  data: PropTypes.shape({
    img: PropTypes.string,
    title: PropTypes.string,
    details: PropTypes.string,
  }),
  onClose: PropTypes.func,
};

Details.defaultProps = {
  data: {},
};

export default Details;
