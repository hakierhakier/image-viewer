import React from 'react';
import CenteredContent from '../layouts/CenteredContent';

const Fetching = () => <CenteredContent content={'fetching...'} />;

export default Fetching;
