import { Machine, assign } from 'xstate';
import data from './data-mock';

const appMachine = Machine({
  context: {
    data: {},
    viewData: undefined,
  },
  initial: 'fetching',
  states: {
    fetching: {
      after: {
        1000: {
          actions: [assign({ data })],
          target: 'grid',
        },
      },
    },
    grid: {
      entry: [
        assign({
          viewData: context => Object.values(context.data),
        }),
      ],
      on: {
        SELECTED: 'details',
      },
    },
    details: {
      entry: [assign({ viewData: (context, event) => context.data[event.payload.id] })],
      on: {
        CLOSED: 'grid',
      },
    },
  },
});

export default appMachine;
