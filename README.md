# Image viewer

There are two views implemented: grid and details.
There is no any state container (like redux) used since it is quite simple
application. The logic is contained in the App component.

Project structure is divided to three main folders:
- `atoms` containing smallest component units
- `layouts` containing components responsible to structure the content
- `views` combining atoms and layouts to composing the view based on the
received data

In case of need of opening details view as an overlay the effort is not
big since the atomic components and details layout supposed to be reused
and rendered above the content.

#### Additional implementation

After main implementation has been finished there were few things that have
ben introduced:
- application logic has been moved to the state machine.
- fetching view has been introduced

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
